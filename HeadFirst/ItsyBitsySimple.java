import org.jfugue.Player;


/**
 * This program plays a simple version of "Itsy Bitsy Spider." Though not
 * specified, the song is in 6/8 time and in the key of F major. The song
 * plays only the melody, with code duplication.
 */
public class ItsyBitsySimple {
  public static void main(String[] args) {
    Player player = new Player();
    player.play(
        // "Itsy, bitsy spider, climbed up the water spout."
        "F5q F5i F5q G5i A5q. A5q A5i G5q F5i G5q A5i F5q. Rq. " +
            // "Down came the rain and washed the spider out."
            "A5q. A5q Bb5i C6q. C6q. Bb5q A5i Bb5q C6i A5q. Rq. " +
            // "Out came the sun and dried up all the rain, so the"
            "F5q. F5q G5i A5q. A5q. G5q F5i G5q A5i F5q. C5q C5i " +
            // "itsy, bitsy spider went up the spout again."
            "F5q F5i F5q G5i A5q. A5q A5i G5q F5i G5q A5i F5q. Rq."
    );
  }
}
