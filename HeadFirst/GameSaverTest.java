

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class GameSaverTest
{
    public static void main (String[] args) {
        GameCharacter one = new GameCharacter(50, "Andrea", new String[] {"UNCO, ", "glasses, ", "Aiden Rosa, ", "gun"});
        GameCharacter two = new GameCharacter(200, "Jack Ploetz", new String[] {"Humble, ", "drill"});
        GameCharacter three = new GameCharacter(120, "Aiden Rosa", new String[] {"Harry Potter Fans, ", "Earbuds"});
        GameCharacter four = new GameCharacter(800, "Silas", new String[] {"jug, ", "creatine"});
        GameCharacter five = new GameCharacter(10000, "JFK", new String[] {"Beasting, ", "Hustling"});

        try {
            ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("Game.ser"));
            os.writeObject(one);
            os.writeObject(two);
            os.writeObject(three);
            os.writeObject(four);
            os.writeObject(five);
            os.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }

        one = null;
        two = null;
        three = null;
        four = null;
        five = null;

        try {
            ObjectInputStream is = new ObjectInputStream(new FileInputStream("Game.ser"));
            GameCharacter oneRestore = (GameCharacter) is.readObject();
            GameCharacter twoRestore = (GameCharacter) is.readObject();
            GameCharacter threeRestore = (GameCharacter) is.readObject();
            GameCharacter fourRestore = (GameCharacter) is.readObject();
            GameCharacter fiveRestore = (GameCharacter) is.readObject();

            System.out.println("One's type: " + oneRestore.getType()+" //weapons:"+oneRestore.getWeapons()+" //power:"+oneRestore.getPower());
            System.out.println("Two's type: " + twoRestore.getType()+" //weapons:"+twoRestore.getWeapons()+" //power:"+twoRestore.getPower());
            System.out.println("Three's type: " + threeRestore.getType()+" //weapons:"+threeRestore.getWeapons()+" //power:"+threeRestore.getPower());
            System.out.println("Four's type: " + fourRestore.getType()+" //weapons:"+fourRestore.getWeapons()+" //power:"+fourRestore.getPower());
            System.out.println("Five's type: " + fiveRestore.getType()+" //weapons:"+fiveRestore.getWeapons()+" //power:"+fiveRestore.getPower());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
