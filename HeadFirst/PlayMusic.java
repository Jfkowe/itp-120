package playmusic;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.swing.JOptionPane;
import sun.audio.AudioPlayer;
import sun.audio.AudioStream;


public class PlayMusic {

  public static void main(String[] args){

    PlayMusic("samovar-party.mp3");
  }

  public static void playMusic(String filepath){
    InputStream music;
    try{
      music = new FileInputStream(new File(filepath));
      AudioStream audios= new AudioStream(music);
      AudioPlayer.player.start(audios);
    }
    catch(Exception e){
      JOptionPane.showMessageDialog(null, "error");
    }
  }
}
