import javax.sound.midi.*;

import java.util.concurrent.TimeUnit;

public class MidiSong
{				// this is the first one
  public static void main (String[]args)
  {
    MiniMusicCmdLine mini = new MiniMusicCmdLine ();
    if (args.length < 2)
      {
	System.out.
	  println ("Don't forget the instrument and note args");
      }
    else
      {
	int instrument = Integer.parseInt (args[0]);
	int note = Integer.parseInt (args[1]);
  int g = note;
  for (int x =0;x<6;x++){ //creates a for loop to play many different notes
    mini.play (instrument, note); //invokes play method
    note = note - 2; //increments tone
    if (x==2){
      try {TimeUnit.MILLISECONDS.sleep(200);}
      catch (InterruptedException e){
        e.printStackTrace();
      }
    }
    if (note < 43){
      note = 47;
    }
    try {TimeUnit.MILLISECONDS.sleep(300);}
    catch (InterruptedException e){
      e.printStackTrace();
    }
  }
  try {TimeUnit.MILLISECONDS.sleep(300);}
  catch (InterruptedException e){
    e.printStackTrace();
  }
    for (int x =0;x<4;x++){
      note = 43;
      mini.play (instrument, note);
      try {TimeUnit.MILLISECONDS.sleep(250);}
      catch (InterruptedException e){
        e.printStackTrace();
      }

      }
      try {TimeUnit.MILLISECONDS.sleep(200);}
      catch (InterruptedException e){
        e.printStackTrace();
      }
      for (int x =0;x<4;x++){
        note = 45;
        mini.play (instrument, note);
        try {TimeUnit.MILLISECONDS.sleep(250);}
        catch (InterruptedException e){
          e.printStackTrace();
        }

        }
        try {TimeUnit.MILLISECONDS.sleep(200);}
        catch (InterruptedException e){
          e.printStackTrace();
        }
        note = 47;
        for (int x =0;x<4;x++){
           //creates a for loop to play many different notes
          mini.play (instrument, note); //invokes play method
          note = note - 2; //increments tone
          if (note < 43){
            note = 47;
          }
          try {TimeUnit.MILLISECONDS.sleep(300);}
          catch (InterruptedException e){
            e.printStackTrace();
          }
        }
      System.exit(0);
    }
  }				// close main
  public void play (int instrument, int note)
  {
    try
    {
      Sequencer player = MidiSystem.getSequencer ();
      player.open ();
      Sequence seq = new Sequence (Sequence.PPQ, 4);
      Track track = seq.createTrack ();
      MidiEvent event = null;
      ShortMessage first = new ShortMessage ();
      first.setMessage (192, 1, instrument, 0);
      MidiEvent changeInstrument = new MidiEvent (first, 1);
      track.add (changeInstrument);
      ShortMessage a = new ShortMessage ();
      a.setMessage (144, 1, note, 100);
      MidiEvent noteOn = new MidiEvent (a, 1);
      track.add (noteOn);
      ShortMessage b = new ShortMessage ();
      b.setMessage (128, 1, note, 100);
      MidiEvent noteOff = new MidiEvent (b, 16);
      track.add (noteOff);
      player.setSequence (seq);
      player.start ();
       //tells you how long to play each note
    } catch (Exception ex)
    {
      ex.printStackTrace ();
  }}
}
